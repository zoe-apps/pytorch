# Copyright (c) 2016, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Jupyter Zoe application description generator."""

import json
import sys
import os

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

UPSTREAM_NOTEBOOKS = [
    ("pytorch-notebook", REPOSITORY + "/pytorch:" + VERSION, []),
    ("pytorch-nb-gpu", REPOSITORY + "/pytorch:" + VERSION, ['gpu'])
]

ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'core_limit': {
        'value': 4,
        'description': 'Notebook core limit'
    },
    'memory_limit': {
        'value': 4 * (1024**3),
        'description': 'Notebook memory limit (bytes)'
    }
}

def jupyter_service(memory_limit, core_limit, image, labels):
    """
    :rtype: dict
    """
    service = {
        'name': "jupyter",
        'image': image,
        'monitor': True,
        'resources': {
            "memory": {
                "min": memory_limit,
                "max": memory_limit
            },
            "cores": {
                "min": core_limit,
                "max": core_limit
            }
        },
        'ports': [
            {
                'name': 'Jupyter Notebook interface',
                'protocol': 'tcp',
                'port_number': 8888,
                'url_template': 'http://{ip_port}/'
            }
        ],
        'environment': [
            ['NB_UID', '1000'],
            ['HOME', '/mnt/workspace']
        ],
        'volumes': [],
        'command': None,
        'work_dir': '/mnt/workspace',
        'total_count': 1,
        'essential_count': 1,
        'replicas': 1,
        'startup_order': 0
    }
    if len(labels) > 0:
        service['labels'] = labels

    return service


if __name__ == '__main__':
    for app_name, image, labels in UPSTREAM_NOTEBOOKS:
        app = {
            'name': app_name,
            'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
            'will_end': False,
            'size': 512,
            'services': [
                jupyter_service(options["memory_limit"]["value"], options["core_limit"]["value"], image, labels)
            ]
        }

        json.dump(app, open(app_name + ".json", "w"), sort_keys=True, indent=4)

    print("ZApps written")

